package com.universe360.www.taskreminder.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by Jimit Patel on 11/12/15.
 * <p>This is the wrapper class for all RecyclerAdapter to reduce the re-coding of certain repeated code</p>
 */
public abstract class RecyclerWrapperAdapter<E> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected Context context;
    protected List<E> objects;

    public void setContext(Context context) {
        this.context = context;
    }

    public void setObjects(List<E> objects) {
        this.objects = objects;
        notifyDataSetChanged();
    }

    public void add(@NonNull E object) {
        objects.add(object);
        notifyDataSetChanged();
    }

    public void add(int position, @NonNull E object) {
        if (position < objects.size() && position >= 0) {
            objects.add(position, object);
            notifyItemChanged(position);
            notifyDataSetChanged();
        } else if (position >= objects.size()) {
            objects.add(object);
            notifyDataSetChanged();
        }
    }

    public void set(int position, @NonNull E object) {
        if (position < objects.size() && position >= 0) {
            objects.set(position, object);
            notifyItemChanged(position);
        } else if (position >= objects.size()) {
            objects.add(object);
            notifyDataSetChanged();
        }
    }

    public void remove(@NonNull E object) {
        objects.remove(object);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (position >=0 && position < objects.size()) {
            objects.remove(position);
            notifyDataSetChanged();
        }
    }

    public void removeAll() {
        objects.clear();
        notifyDataSetChanged();
    }

    public E getItem(int position) {
        return objects.get(position);
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }
}
