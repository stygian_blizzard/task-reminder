package com.universe360.www.taskreminder;

import java.util.Calendar;

/**
 * Created by jimitpatel on 19/03/16.
 */
public interface CalendarItemClickListener {
    void onOffCalendarMonthSelection(int year, int month, Calendar calendar);
    void changeMonth(int offset);
}
