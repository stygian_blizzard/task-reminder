package com.universe360.www.taskreminder.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.universe360.www.taskreminder.BuildConfig;
import com.universe360.www.taskreminder.fragment.CalendarFragment;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by jimitpatel on 17/03/16.
 */
public class MonthPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = MonthPagerAdapter.class.getSimpleName();

    private static final Date INITIAL_DATE = new Date(0);
    private static final Calendar INITIAL_CALENDAR = Calendar.getInstance();
    static {
        INITIAL_CALENDAR.setTime(INITIAL_DATE);
    }
    public static final int FIRST_YEAR = INITIAL_CALENDAR.get(Calendar.YEAR);
    public static final int MONTH_IN_YEAR = 12;

    public MonthPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        int year = FIRST_YEAR + (position / MONTH_IN_YEAR);
        int month = (position % MONTH_IN_YEAR);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "getItem: month=" + month + ", year=" + year);
        CalendarFragment fragment = CalendarFragment.getInstance(year, month);
        return fragment;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
