package com.universe360.www.taskreminder.model;

import java.util.Calendar;

/**
 * Created by jimitpatel on 17/03/16.
 */
public class Day {

    public static final int VIEW_STATIC_DAYS = 1;
    public static final int VIEW_DAYS = 2;
    public static final int VIEW_OTHER_MONTH = 3;

    private String display;
    private Calendar calendar;
    private boolean isCurrentDate;
    private boolean isSelected;
    private int viewType;

    public Day(String display, Calendar calendar, boolean isCurrentDate, boolean isSelected, int viewType) {
        this.display = display;
        this.calendar = calendar;
        this.isCurrentDate = isCurrentDate;
        this.isSelected = isSelected;
        this.viewType = viewType;
    }

    public String getDisplay() {
        return display;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public boolean isCurrentDate() {
        return isCurrentDate;
    }

    public void setIsCurrentDate(boolean isCurrentDate) {
        this.isCurrentDate = isCurrentDate;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getViewType() {
        return viewType;
    }
}
