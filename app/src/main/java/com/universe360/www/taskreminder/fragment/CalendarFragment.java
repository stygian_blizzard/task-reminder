package com.universe360.www.taskreminder.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.universe360.www.taskreminder.BuildConfig;
import com.universe360.www.taskreminder.CalendarItemClickListener;
import com.universe360.www.taskreminder.R;
import com.universe360.www.taskreminder.adapter.CalendarAdapter;
import com.universe360.www.taskreminder.model.Day;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {

    private static final String TAG = CalendarFragment.class.getSimpleName();
    public static final String YEAR = "year";
    public static final String MONTH = "month";

    private Activity activity;
    private int mYear;
    private int mMonth;
    private CalendarAdapter mAdapter;
    private CalendarItemClickListener mListener;

    private View mView;
    private RecyclerView recyclerView;

    public static CalendarFragment getInstance(int year, int month) {
        CalendarFragment fragment = new CalendarFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(YEAR, year);
        bundle.putInt(MONTH, month);
        fragment.setArguments(bundle);
        return fragment;
    }

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (CalendarItemClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement CalendarItemClickListener interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVals();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_calendar, container, false);
        initViews();
        getMonthDetail();
        return  mView;
    }


    private void initVals() {
        activity = getActivity();
        retrieveBundle();
        mAdapter = new CalendarAdapter(activity, new ArrayList<Day>(), mYear, mMonth, mListener);
    }

    private void retrieveBundle() {
        Bundle bundle = getArguments();
        if (null == bundle)
            throw new NullPointerException("Requires month and year values in bundle");
        mYear = bundle.getInt(YEAR);
        mMonth = bundle.getInt(MONTH);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "retrieveBundle: month=" + mMonth + ", year=" + mYear);
    }

    private void initViews() {
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 7);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mAdapter);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mAdapter.getItemViewType(position)) {
                    case Day.VIEW_STATIC_DAYS:
                    case Day.VIEW_OTHER_MONTH:
                    case Day.VIEW_DAYS:
                        return 1;
                    default:
                        return gridLayoutManager.getSpanCount();
                }
            }
        });

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, mYear);
        calendar.set(Calendar.MONTH, mMonth);
    }

    private void getMonthDetail() {
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, mYear);
        calendar.set(Calendar.MONTH, mMonth);

        Day day1 = new Day("S", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day1);
        Day day2 = new Day("M", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day2);
        Day day3 = new Day("T", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day3);
        Day day4 = new Day("W", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day4);
        Day day5 = new Day("T", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day5);
        Day day6 = new Day("F", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day6);
        Day day7 = new Day("S", calendar, false, false, Day.VIEW_STATIC_DAYS);
        mAdapter.add(day7);

        for (int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            Calendar subCalendar = Calendar.getInstance();
            subCalendar.set(Calendar.YEAR, mYear);
            subCalendar.set(Calendar.MONTH, mMonth);
            subCalendar.set(Calendar.DATE, i);

            if (i == 1) {
                // To fill the previous month's date
                int dayOfWeek = subCalendar.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek > 0) {
                    Calendar offCalendar = Calendar.getInstance();
                    offCalendar.set(Calendar.YEAR, subCalendar.get(Calendar.MONTH) == Calendar.JANUARY ? mYear - 1 : mYear);
                    offCalendar.set(Calendar.MONTH, subCalendar.get(Calendar.MONTH) == Calendar.JANUARY ? Calendar.DECEMBER : mMonth - 1);

                    for (int j = dayOfWeek - 1; j > 0; j--) {
                        offCalendar.set(Calendar.DATE, offCalendar.getActualMaximum(Calendar.DATE) - j + 1);
                        Day day = new Day(String.valueOf(offCalendar.get(Calendar.DATE)), offCalendar, false, false, Day.VIEW_OTHER_MONTH);
                        mAdapter.add(day);
                    }
                }
            }

            // Current month's data
            boolean isTodayDate = subCalendar.get(Calendar.DATE) == today.get(Calendar.DATE)
                    && subCalendar.get(Calendar.MONTH) == today.get(Calendar.MONTH)
                    && subCalendar.get(Calendar.YEAR) == today.get(Calendar.YEAR);

            Day day = new Day(String.valueOf(i), subCalendar, isTodayDate, false, Day.VIEW_DAYS);
            mAdapter.add(day);

            if (i == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                // Next month data to fill the space
                int dayOfWeek = subCalendar.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek < 7) {
                    int d = 1;
                    Calendar offCalendar = Calendar.getInstance();
                    offCalendar.set(Calendar.YEAR, subCalendar.get(Calendar.MONTH) == Calendar.DECEMBER ? mYear + 1 : mYear);
                    offCalendar.set(Calendar.MONTH, subCalendar.get(Calendar.MONTH) == Calendar.DECEMBER ? Calendar.JANUARY : mMonth + 1);
                    for (int j = dayOfWeek + 1; j <= 7; j++) {
                        offCalendar.set(Calendar.DATE, d++);
                        Day offDay = new Day(String.valueOf(offCalendar.get(Calendar.DATE)), offCalendar, false, false, Day.VIEW_OTHER_MONTH);
                        mAdapter.add(offDay);
                    }
                }
            }
        }
    }
}
