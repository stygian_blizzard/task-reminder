package com.universe360.www.taskreminder.utils;

import android.app.Activity;
import android.database.Cursor;
import android.provider.CalendarContract;
import android.util.Log;

import com.universe360.www.taskreminder.BuildConfig;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jimitpatel on 17/03/16.
 */
public class CalendarUtil {

    private static final String TAG = CalendarUtil.class.getSimpleName();

    public static int compareTo(int year, int month, Calendar otherCalendar) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);

        return otherCalendar.compareTo(calendar);
    }

    public static void getEvents(Activity activity, Calendar calendar) {
        String[] projection = new String[]{CalendarContract.Events.CALENDAR_ID, CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND,
                CalendarContract.Events.ALL_DAY, CalendarContract.Events.EVENT_LOCATION};

        String selection = "STRFTIME('%Y-%m-%d', " + calendar.getTimeInMillis() + "/1000, 'UNIXEPOCH') BETWEEN " +
                " STRFTIME('%Y-%m-%d', " + CalendarContract.Events.DTSTART + "/1000, 'UNIXEPOCH') " +
                " AND STRFTIME('%Y-%m-%d', " + CalendarContract.Events.DTSTART + "/1000, 'UNIXEPOCH')";

        if (!AppPermission.isPermissionRequestRequired(activity, new String[]{"android.permission.READ_CALENDAR"},
                100)) {
            Cursor cursor = activity.getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                    projection, selection, null, null);

            if (null != cursor && cursor.moveToFirst()) {
                do {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getEvents: id=" + cursor.getString(0) + ", title=" + cursor.getString(1)
                                + ", description=" + cursor.getString(2) + ", DTStart=" +  getDate(cursor.getLong(3), "dd/MM/yyyy")
                                + ", DTEnd=" + getDate(cursor.getLong(4), "dd/MM/yyyy") + ", AllDay=" + cursor.getString(5)
                                + ", Location=" + cursor.getString(6));
                    }
                } while (cursor.moveToNext())   ;
                cursor.close();
            }
        }

    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
