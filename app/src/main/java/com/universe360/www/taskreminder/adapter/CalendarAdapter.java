package com.universe360.www.taskreminder.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.universe360.www.taskreminder.BuildConfig;
import com.universe360.www.taskreminder.CalendarItemClickListener;
import com.universe360.www.taskreminder.R;
import com.universe360.www.taskreminder.model.Day;
import com.universe360.www.taskreminder.utils.CalendarUtil;
import com.universe360.www.taskreminder.widgets.SquareRelativeLayout;

import java.util.ArrayList;

/**
 * Created by jimitpatel on 18/03/16.
 */
public class CalendarAdapter extends RecyclerWrapperAdapter<Day> {

    private static final String TAG = CalendarAdapter.class.getSimpleName();

    private int previousPosition;
    private int year;
    private int month;
    private CalendarItemClickListener mListener;

    public CalendarAdapter(Context context, ArrayList<Day> objects, int year, int month, CalendarItemClickListener listner) {
        this.context = context;
        this.objects = objects;
        this.mListener = listner;

        this.year = year;
        this.month = month;
    }

    @Override
    public int getItemViewType(int position) {
        return objects.get(position).getViewType();
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case Day.VIEW_STATIC_DAYS: {
                return new ViewHolderDays(LayoutInflater.from(context).inflate(R.layout.listitem_calendar_week, parent, false));
            }
            case Day.VIEW_DAYS: {
                return new ViewHolderDays(LayoutInflater.from(context).inflate(R.layout.listitem_calendar_days, parent, false));
            }
            case Day.VIEW_OTHER_MONTH: {
                return new ViewHolderDays(LayoutInflater.from(context).inflate(R.layout.listitem_calendar_off_days, parent, false));
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolderDays) {
            ViewHolderDays holder = (ViewHolderDays) viewHolder;
            holder.lbl.setText(objects.get(position).getDisplay());
            holder.root.setChecked(objects.get(position).isSelected());

            switch (getItemViewType(position)) {
                case Day.VIEW_DAYS: {

                    if (objects.get(position).isCurrentDate() && !objects.get(position).isSelected()) {
                        holder.lbl.setBackground(context.getResources().getDrawable(R.drawable.rectangle_regbg_corner));
                        holder.lbl.setTextColor(context.getResources().getColor(R.color.white));
                    } else {
                        if (holder.root.isChecked()) {
                            holder.lbl.setTextColor(context.getResources().getColor(R.color.white));
                        } else {
                            holder.lbl.setTextColor(context.getResources().getColor(R.color.black));
                        }
                    }
                    break;
                }
            }

            holder.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != v.getTag()) {
                        int position = (int) v.getTag();
                        if (BuildConfig.DEBUG)
                            Log.d(TAG, "onClick: " + position);
                        switch (getItemViewType(position)) {
                            case Day.VIEW_DAYS: {
                                objects.get(previousPosition).setIsSelected(false);
                                objects.get(position).setIsSelected(true);
                                notifyDataSetChanged();
                                previousPosition = position;
                                break;
                            }
                            case Day.VIEW_OTHER_MONTH: {
                                if (BuildConfig.DEBUG)
                                    Log.d(TAG, "onClick: year=" + year + ", month=" + month);
                                mListener.onOffCalendarMonthSelection(year, month, objects.get(position).getCalendar());
                                break;
                            }
                        }
                        CalendarUtil.getEvents(((Activity) context), objects.get(position).getCalendar());
                    }
                }
            });

            holder.root.setTag(position);
            holder.lbl.setTag(position);
        }
    }

    class ViewHolderDays extends RecyclerView.ViewHolder {

        protected SquareRelativeLayout root;
        protected TextView lbl;

        public ViewHolderDays(View itemView) {
            super(itemView);
            root = (SquareRelativeLayout) itemView.findViewById(R.id.root);
            lbl = (TextView) itemView.findViewById(android.R.id.text1);
        }
    }
}
