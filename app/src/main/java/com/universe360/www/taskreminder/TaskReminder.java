package com.universe360.www.taskreminder;

import android.app.Application;
import android.util.Log;

/**
 * Created by jimitpatel on 11/03/16.
 */
public class TaskReminder extends Application {
    private static final String TAG = TaskReminder.class.getSimpleName();

    private static TaskReminder _mInstance = new TaskReminder();

    public static TaskReminder getInstance() {
        return _mInstance;
    }

    private TaskReminder() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onCreate: called");
    }
}
