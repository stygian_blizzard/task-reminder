package com.universe360.www.taskreminder.activities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.universe360.www.taskreminder.BuildConfig;
import com.universe360.www.taskreminder.CalendarItemClickListener;
import com.universe360.www.taskreminder.R;
import com.universe360.www.taskreminder.adapter.MonthPagerAdapter;
import com.universe360.www.taskreminder.fragment.CalendarFragment;
import com.universe360.www.taskreminder.utils.AppPermission;
import com.universe360.www.taskreminder.utils.CalendarUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarActivity extends AppCompatActivity implements CalendarItemClickListener {

    private static final String TAG = CalendarActivity.class.getSimpleName();

    private Context mContext;
    private MonthPagerAdapter mAdapter;
    private Fragment currentFragment;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        initVals();
        if (!AppPermission.isPermissionRequestRequired(this, new String[]{"android.permission.READ_CALENDAR"},
                100)) {
            initViews();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 100:
                if (permissions.length == 1) {
                    if (permissions[0].equalsIgnoreCase("android.permission.READ_CALENDAR")
                            && PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                        initViews();
                    } else {
                        if (!AppPermission.isPermissionRequestRequired(this, new String[]{"android.permission.READ_CALENDAR"},
                                100)) {
                            initViews();
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onOffCalendarMonthSelection(int year, int month, Calendar calendar) {
        if (null != viewPager) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "onOffCalendarMonthSelection: " + CalendarUtil.compareTo(year, month, calendar));

            int position = viewPager.getCurrentItem() + CalendarUtil.compareTo(year, month, calendar);
            if (position >= 0)
                viewPager.setCurrentItem(position, true);
        }
    }

    @Override
    public void changeMonth(int offset) {
        if (null != viewPager) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "changeMonth: " + offset);

            int position = viewPager.getCurrentItem() + offset;
            if (position >= 0)
                viewPager.setCurrentItem(position, true);
        }
    }

    private void initVals() {
        mContext = CalendarActivity.this;
    }

    private void initViews() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        mAdapter = new MonthPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);

        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "run: position=" + ((calendar.get(Calendar.YEAR) - MonthPagerAdapter.FIRST_YEAR) * 12) + calendar.get(Calendar.MONTH));
                viewPager.setCurrentItem(((calendar.get(Calendar.YEAR) - MonthPagerAdapter.FIRST_YEAR) * 12) + calendar.get(Calendar.MONTH));
            }
        }, 100);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentFragment = mAdapter.getItem(position);
                Bundle bundle = currentFragment.getArguments();
                int year = bundle.getInt(CalendarFragment.YEAR);
                int month = bundle.getInt(CalendarFragment.MONTH);

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                String sMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

                if (null != getSupportActionBar())
                    getSupportActionBar().setTitle(sMonth + " - " + String.valueOf(year));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
